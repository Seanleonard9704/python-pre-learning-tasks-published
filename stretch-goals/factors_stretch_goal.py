def factors(input):
    # ==============
    # Your code here
    x=[]
    for i in range(2, input +1):
        if input % i == 0 and input != i:
            x.append(i)
            
    if input !=1 and len(x) == 0:
        return str(input) + " is a prime number"
        
        
    else:
        return x
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
