def calculator(number1, number2, operator):
    # ==============
    # Your code here
    import math
    
    x=[]
    
    if operator == "+":
        ans = number1 + number2
    elif operator == "-":
        ans = number1 - number2
    elif operator == "*":
        ans = number1 * number2
    elif operator == "/":
        ans = number1/number2
    
    i = abs(round(ans))

    while i > 0:
        y = i % 2
        i = math.floor(i/2)
        x.append(y)
        
    
    reverse_list = x[::-1]
    convertToString = [str(integer) for integer in reverse_list]
    removeFormat = "".join(convertToString)
    
    
    finalConvert = int(removeFormat)

    return finalConvert
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
