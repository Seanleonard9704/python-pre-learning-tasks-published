def vowel_swapper(string):
    # ==============
    # Your code here
    count={"a":0,"e":0,"i":0,"o":0,"u":0}
    new_String = ""
    
    for i in string:
        if i == 'a' or i=='A':
            count['a'] += 1
            if count['a'] == 2:
                new_String += '4'
            else:
                new_String += i
        elif i =='e' or i=='E':
            count['e'] +=1
            if count['e'] == 2:
                new_String += '3'
            else:
                new_String += i
        elif i=='i' or i=='I':
            count['i'] += 1
            if count['i'] ==2:
                new_String += '!'
            else:
                new_String += i
        elif i =='o' or i =='O':
            count['o'] +=1
            if count['o'] == 2:
                if i == 'o':
                    new_String += 'ooo'
                else:
                    new_String += '000'
            else:
                new_String += i
        elif i=='u' or i=='U':
            count['u']+=1
            if count['u'] ==2:
                new_String += '|_|'
            else:
                new_String += i
        else:
            new_String +=i
    
    
    return new_String

    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a4a e3e i!i o000o u|_|u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av4!lable" to the console

